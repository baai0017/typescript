/** Class representing a Playing Grid */
class PlayingGrid {

    private _grid: GameItem[] = [];
    private mouseLocation: number;
    private gameStart: boolean;
    private owlInventory: GameItem[] = [];
    private isMouseDead: boolean;

    /**
    * Create a Game Item.
    * @param {number} numberOfGridItems - The number of Grid Items value.
    */
    constructor(numberOfGridItems: number) {

        this.gameStart = false;
        this.isMouseDead = false;

        //fill the array with default grassitems
        for (let index = 1; index <= numberOfGridItems; index++) {
            this._grid[index] = new GameItem('grass', index);
        }

    }

    /**
     * @returns
     */
    public getMouseLocation(): number {
        return this.mouseLocation;
    }

    /**
     * Function to create an Grid of Grid Items
     * @param {HTMLElement} gridPosition 
     */
    public render(gridPosition: HTMLElement) {

        const playingField = `
            <div class="gridElements">
                ${this._grid.map(function (tile) {

                let domElement = `<div class="tile" id="${tile.id}"><img src="./assets/img/${tile.name}.png" /></div>`;

                return domElement;

            }).join("")}
            </div>`;
        gridPosition.innerHTML = playingField;

        this.gameStart = true;
    }

    /**
     * 
     */
    public renderOwlInventory(): void {

        let arrayOfPossibilities = ['apple', 'fox', 'owl'];

        //init owl inventory
        if (!this.gameStart) {
            for (let i = 0; i < 3; i++) {
                let randomOwlItem: string = arrayOfPossibilities[Math.floor(Math.random() * arrayOfPossibilities.length)];
                this.owlInventory[i] = new GameItem(randomOwlItem);
            }
        }

        //add new random item in owl inventory
        if (this.owlInventory.length < 3) {
            let randomOwlItem: string = arrayOfPossibilities[Math.floor(Math.random() * arrayOfPossibilities.length)];
            this.owlInventory.push(new GameItem(randomOwlItem));
        }

        //render
        this.renderDomOwlInventory();

    }

    public renderDomOwlInventory(): void {

        let domOwlInventoryGrid = document.getElementById("owl-inventory");

        const domOwlInventory = `
            ${this.owlInventory.map(function (owlIventoryItem) {
                return `<div><img src="./assets/img/${owlIventoryItem.name}.png" alt="${owlIventoryItem.name}"></div>`;
            }).join("")}
            `;

        domOwlInventoryGrid.innerHTML = domOwlInventory;
    }

    /**
     * 
     */
    public owlPlaceRandomTile(): void {

        let randomTile: number = this.randTile(1, 4);

        //Check not to overlap other items instead of grass
        if (this._grid[randomTile] && this._grid[randomTile].name == 'grass') {

            //Check if array is not empty
            if (this.owlInventory.length) {

                //remove first item from the inventory array
                let firstItemFromOwlsIventory: GameItem = this.owlInventory.shift();

                //set id
                firstItemFromOwlsIventory.setId(randomTile);

                //set into the grid
                this._grid[randomTile] = firstItemFromOwlsIventory;

                //render owls inventory again.
                this.renderOwlInventory();

            } else {
                // owl inventory is empty
                console.log('owl inventory is empty');
            }

        } else {
            this.owlPlaceRandomTile();
        }

    }

    /**
     * 
     * @param direction 
     */
    public renderMouse(direction: number = 0): boolean {

        if (!this.gameStart) {
            this.mouseLocation = this.randTile(13, 16);
        }

        let currentMouseLocation = this.mouseLocation;

        //Remove current mouse tile and make it grass.
        this.makeTileGrass(currentMouseLocation);

        //Check if we can move the mouse to requested direction
        if (this.checkIfMoveMouseIsPossible(currentMouseLocation += direction)) {

            //if true, move the mouse
            this.moveTheMouse(direction);
            // mouse is moved return true.
            return true;

        } else {

            //Cant go there :(
            console.log("You can't go there!");

        }

        this._grid[this.mouseLocation] = new GameItem('mouse', this.mouseLocation);
        //return false, mouse isnt moved.
        return false;
    }

    /**
     * 
     * @param direction 
     */
    public moveTheMouse(direction: number = 0): void {
        //move mouse
        this.mouseLocation += direction;

        //set mouse obj in grid array
        this._grid[this.mouseLocation] = new GameItem('mouse', this.mouseLocation);
    }

    /**
     * 
     * @param direction 
     */
    public moveItem(direction: number = 0): void {

        //filter out grass, rock and mouse
        let moveableItems: GameItem[] = this._grid.filter(function (item) {
            return item.name === 'owl' || item.name === 'fox' || item.name === 'apple';
        });

        //move the moveable items like apple
        moveableItems.forEach((item, index, object) => {

            let currentItemLocation = item.id;

            let requestedItemLocation = currentItemLocation + 4;
            if (item.name === 'owl') { requestedItemLocation = currentItemLocation + 12; }

            if (this.checkIfItemBetweenGrid(requestedItemLocation)) {
                if (this.checkIfMoveIsPossible(currentItemLocation, requestedItemLocation)) {
                    //Remove current mouse tile and make it grass.
                    this.makeTileGrass(currentItemLocation);
                    //set mouse obj in grid array
                    this._grid[requestedItemLocation] = new GameItem(item.name, requestedItemLocation);
                } else {
                    this._grid[currentItemLocation] = new GameItem(item.name, currentItemLocation);
                }
            } else {
                //remove item from the array out of grid
                object.splice(index, 1);
                this.makeTileGrass(currentItemLocation);
            }

            console.log('moveable items in grid: ' + moveableItems.length);

        });

    }

    /**
     * 
     * @param requestedTile 
     */
    public checkIfItemBetweenGrid(requestedTile: number): boolean {
        if (!this._grid[requestedTile]) {
            return false;
        }
        return true;
    }

    /**
     * 
     * @param tile 
     */
    public checkIfMoveIsPossible(currentTile: number, requestedTile: number): boolean {

        let currentTileName = this._grid[currentTile].name;
        let currentTileId = this._grid[currentTile].id;

        let requestedTileName = this._grid[requestedTile].name;
        let requestedTileId = this._grid[requestedTile].id;

        if (requestedTileId == this.mouseLocation && currentTileName != 'apple') {
            this.isMouseDead = true;
            return false;
        }

        if (!requestedTileName || requestedTileName !== 'grass') {
            return false;
        }

        return true;
    }

    /**
     * 
     */
    public checkIfMouseIsStuck(): boolean {

        let mouseLocation = this.mouseLocation;

        let left = mouseLocation - 1;
        let up = mouseLocation - 4;
        let right = mouseLocation + 1;

        if ((!this.checkIfMoveMouseIsPossible(left)) && (!this.checkIfMoveMouseIsPossible(up)) && (!this.checkIfMoveMouseIsPossible(right))) {
            return true;
        }

        return false;
    }

    /**
     * 
     * @param tile 
     */
    public checkIfMoveMouseIsPossible(tile: number): boolean {

        if (!this._grid[tile]) {
            return false;
        }

        let name = this._grid[tile].name;
        let id = this._grid[tile].id;

        if (name === 'apple') {
            console.log("Mouse eats the big apple.");
            return true;
        }

        if (!name || name != 'grass') {
            return false;
        }

        if (this.mouseLocation == 13 && id == 12) {
            return false;
        }

        if (this.mouseLocation == 12 && id == 13) {
            return false;
        }

        if (this.mouseLocation == 9 && id == 8) {
            return false;
        }

        if (this.mouseLocation == 8 && id == 9) {
            return false;
        }

        if (this.mouseLocation == 5 && id == 4) {
            return false;
        }

        if (this.mouseLocation == 4 && id == 5) {
            return false;
        }

        return true;
    }

    /**
     * 
     */
    public checkIfMouseHasWonTheGame(): boolean {
        if (this.mouseLocation == 1 || this.mouseLocation == 2 || this.mouseLocation == 3 || this.mouseLocation == 4) {
            return true;
        }
        return false;
    }

    /**
     * 
     * @param index 
     */
    public makeTileGrass(index: number): void {
        this._grid[index] = new GameItem('grass', index);
    }

    /**
     * 
     */
    public renderRock(): void {
        const randomTile = this.randTile(5, 12);
        this._grid[randomTile] = new GameItem('rock', randomTile);
    }

    /**
     * 
     * @param min 
     * @param max 
     */
    public randTile(min: number, max: number): number {
        return Math.round(Math.random() * (max - min) + min);
    }

    /**
     * 
     */
    public checkIfMouseIsDead(): boolean {
        return this.isMouseDead;
    }

}