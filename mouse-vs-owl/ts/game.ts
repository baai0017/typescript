/** Class representing a game */
class Game {

    private _playingGrid: PlayingGrid;
    private turnLeft: number;
    private gameOver: boolean = false;
    private turn: boolean;

    /**
     * 
     */
    constructor() {

        this.turnLeft = 8;
        this.renderDomTurnPlayerLeft();

        //mouse turn first.
        this.switchTurns(false);

        this._playingGrid = new PlayingGrid(16);

        this._playingGrid.renderOwlInventory();
        this._playingGrid.renderMouse();
        this._playingGrid.renderRock();

        this.render();

        window.addEventListener("keydown", this.moveMouse);

    }

    /**
     * Renders the game representation in the DOM
     */
    private render() {
        this._playingGrid.render(document.getElementById('grid'));
    }

    private renderDomTurnPlayerLeft() {
        let domTurnleft = document.getElementById("amount-turn-left");
        domTurnleft.innerHTML = this.turnLeft.toString();
    }

    /**
     * 
     */
    public removePlayerAmountOfTurns() {
        if (this.hasPlayerMovesLeft()) {

            this.turnLeft--;
            this.renderDomTurnPlayerLeft();

        } else {
            this.gameOver = true;
        }
    }

    /**
     * 
     */
    public hasPlayerMovesLeft(): boolean {
        if (!this.gameOver && this.turnLeft > 0) {
            return true;
        }
        return false;
    }

    /**
     * 
     */
    public moveMouse = (e: KeyboardEvent) => {

        if (!this.getTurn()) {
            console.log("Wait for your turn!");
            return;
        }

        if (this.hasPlayerMovesLeft()) {
            switch (e.keyCode) {

                case 65:
                case 37:

                    //links
                    if (this._playingGrid.renderMouse(-1)) {
                        //update turns left for the player
                        this.removePlayerAmountOfTurns();
                        //switch turns and call owl move
                        this.switchTurns(this.getTurn());
                    }

                    break;

                case 87:
                case 38:

                    //boven
                    if (this._playingGrid.renderMouse(-4)) {
                        //update turns left for the player
                        this.removePlayerAmountOfTurns();
                        //switch turns and call owl move
                        this.switchTurns(this.getTurn());
                    }

                    break;

                case 68:
                case 39:

                    //rechts
                    if (this._playingGrid.renderMouse(+1)) {
                        //update turns left for the player
                        this.removePlayerAmountOfTurns();
                        //switch turns and call owl move
                        this.switchTurns(this.getTurn());
                    }

                    break;

                default:
                    this._playingGrid.renderMouse();
                    break;
            }

            //Render the game.
            this.render();

            //Check if mouse has won the game
            if (this._playingGrid.checkIfMouseHasWonTheGame()) {
                //mouse has won.
                this.stopGame('win', 'Mouse has crossover the forest.');
                return;
            }

            //check if mouse isnt stuck
            if(this._playingGrid.checkIfMouseIsStuck()) {
                this.stopGame('lose', 'Mouse is stuck.');
                return;
            }

        } else {
            //No more moves left, game over.
            this.stopGame('lose', 'No more moves left.');
            return;
        }

    }

    /**
     * 
     */
    public getTurn(): boolean {
        return this.turn;
    }

    /**
     * 
     * @param turn 
     */
    public switchTurns(turn: boolean) {

        let domTurn = document.getElementById("turn");

        if (turn) {

            this.turn = false;
            domTurn.innerHTML = "owl";
            this._playingGrid.owlPlaceRandomTile();

            //move pieces down the grid then turn back to the player / mouse
            this._playingGrid.moveItem();

            if(this._playingGrid.checkIfMouseIsDead()) {
                //mouse is killed by a fox or owl
                this.stopGame('lose', 'Mouse is killed by an animal.');
            }

            //Switch turn to mouse
            this.switchTurns(false);

            console.log("TURN TO OWL");

        } else {
            this.turn = true;
            domTurn.innerHTML = "mouse";

            console.log("TURN TO MOUSE");
        }

    }


    /**
     * 
     */
    public stopGame(gameState: string, reason: string): void {

        window.removeEventListener("keydown", this.moveMouse);

        if(gameState == 'win') {
            console.log("You win!");
            document.getElementById('win').style.display = "block";
        } else {
            console.log("Game over!");
            document.getElementById('lose').style.display = "block";
            document.getElementById('reason').innerHTML = reason;
        }     
    }



}