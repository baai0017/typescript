var app;
(function () {
    var init = function () {
        app = new Game();
    };
    window.addEventListener('load', init);
})();
var Game = (function () {
    function Game() {
        var _this = this;
        this.gameOver = false;
        this.moveMouse = function (e) {
            if (!_this.getTurn()) {
                console.log("Wait for your turn!");
                return;
            }
            if (_this.hasPlayerMovesLeft()) {
                switch (e.keyCode) {
                    case 65:
                    case 37:
                        if (_this._playingGrid.renderMouse(-1)) {
                            _this.removePlayerAmountOfTurns();
                            _this.switchTurns(_this.getTurn());
                        }
                        break;
                    case 87:
                    case 38:
                        if (_this._playingGrid.renderMouse(-4)) {
                            _this.removePlayerAmountOfTurns();
                            _this.switchTurns(_this.getTurn());
                        }
                        break;
                    case 68:
                    case 39:
                        if (_this._playingGrid.renderMouse(+1)) {
                            _this.removePlayerAmountOfTurns();
                            _this.switchTurns(_this.getTurn());
                        }
                        break;
                    default:
                        _this._playingGrid.renderMouse();
                        break;
                }
                _this.render();
                if (_this._playingGrid.checkIfMouseHasWonTheGame()) {
                    _this.stopGame('win', 'Mouse has crossover the forest.');
                    return;
                }
                if (_this._playingGrid.checkIfMouseIsStuck()) {
                    _this.stopGame('lose', 'Mouse is stuck.');
                    return;
                }
            }
            else {
                _this.stopGame('lose', 'No more moves left.');
                return;
            }
        };
        this.turnLeft = 8;
        this.renderDomTurnPlayerLeft();
        this.switchTurns(false);
        this._playingGrid = new PlayingGrid(16);
        this._playingGrid.renderOwlInventory();
        this._playingGrid.renderMouse();
        this._playingGrid.renderRock();
        this.render();
        window.addEventListener("keydown", this.moveMouse);
    }
    Game.prototype.render = function () {
        this._playingGrid.render(document.getElementById('grid'));
    };
    Game.prototype.renderDomTurnPlayerLeft = function () {
        var domTurnleft = document.getElementById("amount-turn-left");
        domTurnleft.innerHTML = this.turnLeft.toString();
    };
    Game.prototype.removePlayerAmountOfTurns = function () {
        if (this.hasPlayerMovesLeft()) {
            this.turnLeft--;
            this.renderDomTurnPlayerLeft();
        }
        else {
            this.gameOver = true;
        }
    };
    Game.prototype.hasPlayerMovesLeft = function () {
        if (!this.gameOver && this.turnLeft > 0) {
            return true;
        }
        return false;
    };
    Game.prototype.getTurn = function () {
        return this.turn;
    };
    Game.prototype.switchTurns = function (turn) {
        var domTurn = document.getElementById("turn");
        if (turn) {
            this.turn = false;
            domTurn.innerHTML = "owl";
            this._playingGrid.owlPlaceRandomTile();
            this._playingGrid.moveItem();
            if (this._playingGrid.checkIfMouseIsDead()) {
                this.stopGame('lose', 'Mouse is killed by an animal.');
            }
            this.switchTurns(false);
            console.log("TURN TO OWL");
        }
        else {
            this.turn = true;
            domTurn.innerHTML = "mouse";
            console.log("TURN TO MOUSE");
        }
    };
    Game.prototype.stopGame = function (gameState, reason) {
        window.removeEventListener("keydown", this.moveMouse);
        if (gameState == 'win') {
            console.log("You win!");
            document.getElementById('win').style.display = "block";
        }
        else {
            console.log("Game over!");
            document.getElementById('lose').style.display = "block";
            document.getElementById('reason').innerHTML = reason;
        }
    };
    return Game;
}());
var GameItem = (function () {
    function GameItem(name, id) {
        if (id === void 0) { id = null; }
        this._name = name;
        this._id = id;
    }
    Object.defineProperty(GameItem.prototype, "id", {
        get: function () {
            return this._id;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(GameItem.prototype, "name", {
        get: function () {
            return this._name;
        },
        enumerable: true,
        configurable: true
    });
    GameItem.prototype.setId = function (gameItemId) {
        this._id = gameItemId;
    };
    return GameItem;
}());
var PlayingGrid = (function () {
    function PlayingGrid(numberOfGridItems) {
        this._grid = [];
        this.owlInventory = [];
        this.gameStart = false;
        this.isMouseDead = false;
        for (var index = 1; index <= numberOfGridItems; index++) {
            this._grid[index] = new GameItem('grass', index);
        }
    }
    PlayingGrid.prototype.getMouseLocation = function () {
        return this.mouseLocation;
    };
    PlayingGrid.prototype.render = function (gridPosition) {
        var playingField = "\n            <div class=\"gridElements\">\n                " + this._grid.map(function (tile) {
            var domElement = "<div class=\"tile\" id=\"" + tile.id + "\"><img src=\"./assets/img/" + tile.name + ".png\" /></div>";
            return domElement;
        }).join("") + "\n            </div>";
        gridPosition.innerHTML = playingField;
        this.gameStart = true;
    };
    PlayingGrid.prototype.renderOwlInventory = function () {
        var arrayOfPossibilities = ['apple', 'fox', 'owl'];
        if (!this.gameStart) {
            for (var i = 0; i < 3; i++) {
                var randomOwlItem = arrayOfPossibilities[Math.floor(Math.random() * arrayOfPossibilities.length)];
                this.owlInventory[i] = new GameItem(randomOwlItem);
            }
        }
        if (this.owlInventory.length < 3) {
            var randomOwlItem = arrayOfPossibilities[Math.floor(Math.random() * arrayOfPossibilities.length)];
            this.owlInventory.push(new GameItem(randomOwlItem));
        }
        this.renderDomOwlInventory();
    };
    PlayingGrid.prototype.renderDomOwlInventory = function () {
        var domOwlInventoryGrid = document.getElementById("owl-inventory");
        var domOwlInventory = "\n            " + this.owlInventory.map(function (owlIventoryItem) {
            return "<div><img src=\"./assets/img/" + owlIventoryItem.name + ".png\" alt=\"" + owlIventoryItem.name + "\"></div>";
        }).join("") + "\n            ";
        domOwlInventoryGrid.innerHTML = domOwlInventory;
    };
    PlayingGrid.prototype.owlPlaceRandomTile = function () {
        var randomTile = this.randTile(1, 4);
        if (this._grid[randomTile] && this._grid[randomTile].name == 'grass') {
            if (this.owlInventory.length) {
                var firstItemFromOwlsIventory = this.owlInventory.shift();
                firstItemFromOwlsIventory.setId(randomTile);
                this._grid[randomTile] = firstItemFromOwlsIventory;
                this.renderOwlInventory();
            }
            else {
                console.log('owl inventory is empty');
            }
        }
        else {
            this.owlPlaceRandomTile();
        }
    };
    PlayingGrid.prototype.renderMouse = function (direction) {
        if (direction === void 0) { direction = 0; }
        if (!this.gameStart) {
            this.mouseLocation = this.randTile(13, 16);
        }
        var currentMouseLocation = this.mouseLocation;
        this.makeTileGrass(currentMouseLocation);
        if (this.checkIfMoveMouseIsPossible(currentMouseLocation += direction)) {
            this.moveTheMouse(direction);
            return true;
        }
        else {
            console.log("You can't go there!");
        }
        this._grid[this.mouseLocation] = new GameItem('mouse', this.mouseLocation);
        return false;
    };
    PlayingGrid.prototype.moveTheMouse = function (direction) {
        if (direction === void 0) { direction = 0; }
        this.mouseLocation += direction;
        this._grid[this.mouseLocation] = new GameItem('mouse', this.mouseLocation);
    };
    PlayingGrid.prototype.moveItem = function (direction) {
        var _this = this;
        if (direction === void 0) { direction = 0; }
        var moveableItems = this._grid.filter(function (item) {
            return item.name === 'owl' || item.name === 'fox' || item.name === 'apple';
        });
        moveableItems.forEach(function (item, index, object) {
            var currentItemLocation = item.id;
            var requestedItemLocation = currentItemLocation + 4;
            if (item.name === 'owl') {
                requestedItemLocation = currentItemLocation + 12;
            }
            if (_this.checkIfItemBetweenGrid(requestedItemLocation)) {
                if (_this.checkIfMoveIsPossible(currentItemLocation, requestedItemLocation)) {
                    _this.makeTileGrass(currentItemLocation);
                    _this._grid[requestedItemLocation] = new GameItem(item.name, requestedItemLocation);
                }
                else {
                    _this._grid[currentItemLocation] = new GameItem(item.name, currentItemLocation);
                }
            }
            else {
                object.splice(index, 1);
                _this.makeTileGrass(currentItemLocation);
            }
            console.log('moveable items in grid: ' + moveableItems.length);
        });
    };
    PlayingGrid.prototype.checkIfItemBetweenGrid = function (requestedTile) {
        if (!this._grid[requestedTile]) {
            return false;
        }
        return true;
    };
    PlayingGrid.prototype.checkIfMoveIsPossible = function (currentTile, requestedTile) {
        var currentTileName = this._grid[currentTile].name;
        var currentTileId = this._grid[currentTile].id;
        var requestedTileName = this._grid[requestedTile].name;
        var requestedTileId = this._grid[requestedTile].id;
        if (requestedTileId == this.mouseLocation && currentTileName != 'apple') {
            this.isMouseDead = true;
            return false;
        }
        if (!requestedTileName || requestedTileName !== 'grass') {
            return false;
        }
        return true;
    };
    PlayingGrid.prototype.checkIfMouseIsStuck = function () {
        var mouseLocation = this.mouseLocation;
        var left = mouseLocation - 1;
        var up = mouseLocation - 4;
        var right = mouseLocation + 1;
        if ((!this.checkIfMoveMouseIsPossible(left)) && (!this.checkIfMoveMouseIsPossible(up)) && (!this.checkIfMoveMouseIsPossible(right))) {
            return true;
        }
        return false;
    };
    PlayingGrid.prototype.checkIfMoveMouseIsPossible = function (tile) {
        if (!this._grid[tile]) {
            return false;
        }
        var name = this._grid[tile].name;
        var id = this._grid[tile].id;
        if (name === 'apple') {
            console.log("Mouse eats the big apple.");
            return true;
        }
        if (!name || name != 'grass') {
            return false;
        }
        if (this.mouseLocation == 13 && id == 12) {
            return false;
        }
        if (this.mouseLocation == 12 && id == 13) {
            return false;
        }
        if (this.mouseLocation == 9 && id == 8) {
            return false;
        }
        if (this.mouseLocation == 8 && id == 9) {
            return false;
        }
        if (this.mouseLocation == 5 && id == 4) {
            return false;
        }
        if (this.mouseLocation == 4 && id == 5) {
            return false;
        }
        return true;
    };
    PlayingGrid.prototype.checkIfMouseHasWonTheGame = function () {
        if (this.mouseLocation == 1 || this.mouseLocation == 2 || this.mouseLocation == 3 || this.mouseLocation == 4) {
            return true;
        }
        return false;
    };
    PlayingGrid.prototype.makeTileGrass = function (index) {
        this._grid[index] = new GameItem('grass', index);
    };
    PlayingGrid.prototype.renderRock = function () {
        var randomTile = this.randTile(5, 12);
        this._grid[randomTile] = new GameItem('rock', randomTile);
    };
    PlayingGrid.prototype.randTile = function (min, max) {
        return Math.round(Math.random() * (max - min) + min);
    };
    PlayingGrid.prototype.checkIfMouseIsDead = function () {
        return this.isMouseDead;
    };
    return PlayingGrid;
}());
//# sourceMappingURL=main.js.map