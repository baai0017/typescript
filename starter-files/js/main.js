var init = function () {
    render(rocket, 10, 10);
    window.addEventListener('keypress', function (e) {
        console.log(e.keyCode);
        switch (e.keyCode) {
            case 97:
                rocket.move(-30, 0);
                break;
            case 119:
                rocket.move(0, -30);
                break;
            case 100:
                rocket.move(+30, 0);
                break;
            case 115:
                rocket.move(0, +30);
                break;
        }
    });
    window.addEventListener('click', function (e) {
        rocket.moveClick(e.x, e.y);
    });
};
window.addEventListener('load', init);
var rocket = {
    xPos: 0,
    yPos: 0,
    image: './assets/images/rocket.png',
    move: function (xPos, yPos) {
        var img = document.getElementById("rocket");
        img.style.transform = "translate(" + (rocket.xPos + xPos) + "px," + (rocket.yPos + yPos) + "px" + ")";
        this.xPos += xPos;
        this.yPos += yPos;
    },
    moveClick: function (MouseXPos, MouseYPos) {
        var img = document.getElementById("rocket");
        img.style.transform = "translate(" + MouseXPos + "px," + MouseYPos + "px" + ")";
        rocket.xPos = MouseXPos;
        rocket.yPos = MouseYPos;
    }
};
function render(rocket, rocketPosX, rocketPosY) {
    var img = createImgElement(rocket.image, "rocket", "rocket");
    document.getElementById("container").innerHTML = img;
    var rocketYPos = rocket.rocketPosX;
    var rocketXPos = rocket.rocketPosY;
    rocket = document.getElementById("rocket");
    rocket.style.transform = "translate(" + rocketXPos + "px," + rocketYPos + "px" + ")";
}
function calculateDistance(startPoint, endPoint) {
}
function createImgElement(path, className, idName) {
    if (path === void 0) { path = ''; }
    if (className === void 0) { className = ''; }
    if (idName === void 0) { idName = ''; }
    var img = "<img src=\"" + path + "\" class=\"" + className + "\" id=\"" + idName + "\">";
    return img;
}
//# sourceMappingURL=main.js.map