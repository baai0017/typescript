var init = function () {
    var topGoalScoresWC = [
        { firstName: 'Miroslav', lastname: 'Klose', team: 'Germany', goals: 16, matches: 24, tournaments: [2002, 2006, 2010, 2014] },
        { firstName: 'Ronaldo', lastname: '', team: 'Brasil', goals: 15, matches: 19, tournaments: [1998, 2002, 2006] },
        { firstName: 'Gerd', lastname: 'Muller', team: 'Germany', goals: 16, matches: 24, tournaments: [2002, 2006, 2010, 2014] },
        { firstName: 'Just', lastname: 'Fontaine', team: 'France', goals: 14, matches: 13, tournaments: [1970, 1974] },
        { firstName: 'Pele', lastname: '', team: 'Brasil', goals: 12, matches: 14, tournaments: [1958, 1962, 1966, 1970] },
        { firstName: 'Sandor', lastname: 'Kocsis', team: 'Hungary', goals: 11, matches: 5, tournaments: [1954] },
        { firstName: 'Jurgen', lastname: 'Klinsmann', team: 'Germany', goals: 11, matches: 17, tournaments: [1990, 1994, 1998] },
        { firstName: 'Helmut', lastname: 'Rahn', team: 'Germany', goals: 10, matches: 10, tournaments: [1954, 1958] },
        { firstName: 'Gary', lastname: 'Lineker', team: 'England', goals: 10, matches: 12, tournaments: [1986, 1990] },
        { firstName: 'Gabriel', lastname: 'Batistuta', team: 'Argentina', goals: 10, matches: 12, tournaments: [1994, 1998, 2002] },
    ];
    var soccerPlayers = topGoalScoresWC.filter(function (soccer) {
        return (soccer.goals >= 11);
    }).map(function (soccer) {
        return soccer.firstName;
    });
    console.log(soccerPlayers);
    render(rocket, 10, 10);
    window.addEventListener('keypress', function (e) {
        console.log(e.keyCode);
        switch (e.keyCode) {
            case 97:
                rocket.move(-30, 0);
                break;
            case 119:
                rocket.move(0, -30);
                break;
            case 100:
                rocket.move(+30, 0);
                break;
            case 115:
                rocket.move(0, +30);
                break;
        }
    });
    window.addEventListener('click', function (e) {
        rocket.moveClick(e.x, e.y);
    });
};
window.addEventListener('load', init);
var rocket = {
    xPos: 0,
    yPos: 0,
    image: './assets/images/rocket.png',
    move: function (xPos, yPos) {
        var img = document.getElementById("rocket");
        img.style.transform = "translate(" + (rocket.xPos + xPos) + "px," + (rocket.yPos + yPos) + "px" + ")";
        this.xPos += xPos;
        this.yPos += yPos;
    },
    moveClick: function (MouseXPos, MouseYPos) {
        var img = document.getElementById("rocket");
        img.style.transform = "translate(" + MouseXPos + "px," + MouseYPos + "px" + ")";
        rocket.xPos = MouseXPos;
        rocket.yPos = MouseYPos;
    }
};
function render(rocket, rocketPosX, rocketPosY) {
    var img = createImgElement(rocket.image, "rocket", "rocket");
    document.getElementById("container").innerHTML = img;
    var rocketYPos = rocket.rocketPosX;
    var rocketXPos = rocket.rocketPosY;
    rocket = document.getElementById("rocket");
    rocket.style.transform = "translate(" + rocketXPos + "px," + rocketYPos + "px" + ")";
}
function calculateDistance(startPoint, endPoint) {
}
function createImgElement(path, className, idName) {
    if (path === void 0) { path = ''; }
    if (className === void 0) { className = ''; }
    if (idName === void 0) { idName = ''; }
    var img = "<img src=\"" + path + "\" class=\"" + className + "\" id=\"" + idName + "\">";
    return img;
}
//# sourceMappingURL=main.js.map