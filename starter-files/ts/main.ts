let init = function () {

    render(rocket, 10, 10);

    window.addEventListener('keypress', (e: KeyboardEvent) => {
        console.log(e.keyCode);

        switch (e.keyCode) {
            case 97:
                rocket.move(-30, 0);
                break;

            case 119:
                rocket.move(0, -30);
                break;

            case 100:
                rocket.move(+30, 0);
                break;

            case 115:
                rocket.move(0, +30);
                break;
        }
    });

    window.addEventListener('click', (e: MouseEvent) => {
        rocket.moveClick(e.x, e.y);
    });

};

window.addEventListener('load', init);

const rocket = {

    xPos: 0,
    yPos: 0,
    image: './assets/images/rocket.png',

    move: function (xPos: number, yPos: number): void {

        let img = document.getElementById("rocket");

        // img.style.top = rocket.yPos + yPos + "px";
        // img.style.left = rocket.xPos + xPos + "px";   

        img.style.transform = "translate(" + (rocket.xPos + xPos) + "px," + (rocket.yPos + yPos) + "px" + ")";


        this.xPos += xPos;
        this.yPos += yPos;

        // let startPoint = [rocket.xPos, rocket.yPos];
        // let endPoint = [xPos, yPos];

        // calculateDistance(startPoint, endPoint);
    },

    moveClick: function (MouseXPos: number, MouseYPos: number): void {

        //
        let img = document.getElementById("rocket");

        //object.style.transform = "none|transform-functions|initial|inherit"

        img.style.transform = "translate(" + MouseXPos + "px," + MouseYPos + "px" + ")";

        // img.style.top = MouseYPos + "px";
        // img.style.left = MouseXPos + "px";

        rocket.xPos = MouseXPos;
        rocket.yPos = MouseYPos;
    }

}

function render(rocket: any, rocketPosX: number, rocketPosY: number): void {

    //create an image element
    let img = createImgElement(rocket.image, "rocket", "rocket");

    //create an container with a img element in it.
    document.getElementById("container").innerHTML = img;

    //default location rocket image
    let rocketYPos = rocket.rocketPosX;
    let rocketXPos = rocket.rocketPosY;

    //init position
    rocket = document.getElementById("rocket");
    rocket.style.transform = "translate(" + rocketXPos + "px," + rocketYPos + "px" + ")";

}

function calculateDistance(startPoint: number[], endPoint: number[]): void {
    //stelling van pitagoras
}

/**
 * Creates an img element
 * 
 * @param path path of the image
 * @param className add an class to the img element
 * @param idName add an id to the img element
 * 
 * @returns img element
 */
function createImgElement(path: string = '', className: string = '', idName: string = ''): string {
    let img = `<img src="${path}" class="${className}" id="${idName}">`;
    return img;
}

// Verzammeleing
// super sup
// hoe wordt die uitgevoerd?

/*

//Opdracht 2.3

//Zodra je een variable aanmaakt met const moet je die meteen een waarde toekennen
//en kan je die waarde niet meer veranderen. In dit geval kan ik de constName niet meer veranderen
const constName = "Nathanael";
constName = "Henk";

//Met let kan je variablen op een later moment in dezelfde block veranderen van waarde
// zoals hier kan ik de variable letName veranderen van Nathanael naar Henk.
let letName = "Nathanael";
letName = "Henk";

if(true) {
    let lol = "tst";
}

lol = "test"

*/


/*

Opdracht 2.4


const topGoalScoresWC = [
    { firstName: 'Miroslav', lastname: 'Klose', team: 'Germany', goals: 16, matches: 24, tournaments: [2002, 2006, 2010, 2014] },
    { firstName: 'Ronaldo', lastname: '', team: 'Brasil', goals: 15, matches: 19, tournaments: [1998, 2002, 2006] },
    { firstName: 'Gerd', lastname: 'Muller', team: 'Germany', goals: 16, matches: 24, tournaments: [2002, 2006, 2010, 2014] },
    { firstName: 'Just', lastname: 'Fontaine', team: 'France', goals: 14, matches: 13, tournaments: [1970, 1974] },
    { firstName: 'Pele', lastname: '', team: 'Brasil', goals: 12, matches: 14, tournaments: [1958, 1962, 1966, 1970] },
    { firstName: 'Sandor', lastname: 'Kocsis', team: 'Hungary', goals: 11, matches: 5, tournaments: [1954] },
    { firstName: 'Jurgen', lastname: 'Klinsmann', team: 'Germany', goals: 11, matches: 17, tournaments: [1990, 1994, 1998] },
    { firstName: 'Helmut', lastname: 'Rahn', team: 'Germany', goals: 10, matches: 10, tournaments: [1954, 1958] },
    { firstName: 'Gary', lastname: 'Lineker', team: 'England', goals: 10, matches: 12, tournaments: [1986, 1990] },
    { firstName: 'Gabriel', lastname: 'Batistuta', team: 'Argentina', goals: 10, matches: 12, tournaments: [1994, 1998, 2002] },
];

for (let player of topGoalScoresWC) {
    console.log(player.firstName);
}

let soccerPlayers1 = topGoalScoresWC.map(function (soccer) {
    return soccer.firstName + " " + soccer.lastname;
});

console.log(soccerPlayers1);



let soccerPlayers2 = topGoalScoresWC.filter(function (soccer) {
    return (soccer.goals >= 11);
}).map(function (soccer) {
    return soccer.firstName;
});

console.log(soccerPlayers2);

*/

/*

Opdracht 2.5

class Counter {

    private _counter: number = 0;

    constructor() {
        document.getElementById('body').addEventListener('click', this.clickHandler);
    }

    public clickHandler = (e: Event) => {
        this._counter++;
        console.log(this._counter);
    }
}

let clickCount = new Counter();

window.addEventListener('click', (e: MouseEvent) => {
    clickCount.clickHandler(e);
});

*/

/*

    const topGoalScoresWC = [
        { firstName: 'Miroslav', lastname: 'Klose', team:'Germany', goals: 16, matches: 24, tournaments: [2002, 2006, 2010, 2014] },
        { firstName: 'Ronaldo', lastname: '', team: 'Brasil', goals: 15, matches: 19, tournaments: [1998, 2002, 2006] },
        { firstName: 'Gerd', lastname: 'Muller', team: 'Germany', goals: 16, matches: 24, tournaments: [2002, 2006, 2010, 2014] },
        { firstName: 'Just', lastname: 'Fontaine', team: 'France', goals: 14, matches: 13, tournaments: [1970, 1974] },
        { firstName: 'Pele', lastname: '', team: 'Brasil', goals: 12, matches: 14, tournaments: [1958, 1962, 1966, 1970] },
        { firstName: 'Sandor', lastname: 'Kocsis', team: 'Hungary', goals:11, matches: 5, tournaments: [1954] },
        { firstName: 'Jurgen', lastname: 'Klinsmann', team: 'Germany', goals: 11, matches: 17, tournaments: [1990, 1994, 1998] },
        { firstName: 'Helmut', lastname: 'Rahn', team: 'Germany', goals: 10, matches: 10, tournaments: [1954, 1958] },
        { firstName: 'Gary', lastname: 'Lineker', team: 'England', goals: 10, matches: 12, tournaments: [1986, 1990] },    
        { firstName: 'Gabriel', lastname: 'Batistuta', team: 'Argentina', goals: 10, matches: 12, tournaments: [1994, 1998, 2002] },    
    ];

    let arrayCopys = [...topGoalScoresWC];

    let player1 = [{ firstName: 'adfs', lastname: 'afdsfsd', team:'Spain', goals: 14, matches: 24, tournaments: [2002, 2006, 2010, 2014] }];
    let player2 = [{ firstName: 'asdfasdffsad', lastname: 'asfd', team:'Belgium', goals: 16, matches: 24, tournaments: [2002, 2006, 2010, 2014] }];

    arrayCopys.splice(2, 0, ...player1);
    arrayCopys.splice(3, 0, ...player2);

    for (let arrayCopy of arrayCopys) {
       console.log(arrayCopy);
    }

*/

/*

Opdracht 2.7 & 2.8

let container = document.getElementById("container");

//const div = createDomElement({tagName: 'div', attributes: {class:'card'}});
//const p = createDomElement({ tagName: 'p', attributes: { class: 'news', id: 'introduction' }, content: 'The news for today' });
const img = createDomElement({ tagName: 'img', attributes: { class: 'newDiv', id: 'profile_image', src: './assets/images/windsock.png' } });

container.innerHTML = img;

interface domElements {
    tagName: string;
    attributes: attributes;
    content?: string;
}

interface attributes {
    class?: string;
    id?: string;
    src?: string;
}

function createDomElement(input : domElements): string {
    let domElement = `<${input.tagName} ${input.attributes.src ? "src=" + input.attributes.src : ""} ${input.attributes.id ? "id=" + input.attributes.id : ""} ${input.attributes.class ? "class=" + input.attributes.class : ""}>${input.content ? input.content : ""}</${input.tagName}>`;
    return domElement;
}

*/




