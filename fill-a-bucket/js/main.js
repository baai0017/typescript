class Bucket {
    constructor(quantity, limit) {
        this.setQuantity(quantity);
        this.setLimit(limit);
    }
    getQuantity() {
        return this.quantity;
    }
    setQuantity(bucketQuantity) {
        this.quantity = bucketQuantity;
    }
    setLimit(bucketLimit) {
        this.limit = bucketLimit;
    }
    addQuantity(cupAmount) {
        if (this.checkBucketLimit(cupAmount)) {
            this.quantity += cupAmount;
            return 'There is ' + cupAmount + 'ml added to the bucket.';
        }
        else {
            console.log('Bucket is too full!');
            return 'Bucket is too full!';
        }
    }
    checkBucketLimit(cupAmount) {
        if (cupAmount <= (this.limit - this.quantity)) {
            return true;
        }
        return false;
    }
    removeQuantity(bucketAmount) {
        this.quantity -= bucketAmount;
    }
    domRepresentationWater() {
        let waterDiv = document.createElement("div");
        waterDiv.className = 'water';
        waterDiv.style.height = (this.getQuantity() / 25) + 'px';
        return waterDiv;
    }
    domRepresentation() {
        let bucketDiv = document.createElement("div");
        bucketDiv.className = "bucket";
        let waterDiv = this.domRepresentationWater();
        bucketDiv.appendChild(waterDiv);
        return bucketDiv;
    }
}
class Cup {
    constructor(quantity, id) {
        this.setQuantity(quantity);
        this.setId(id);
    }
    getId() {
        return this.id;
    }
    getQuantity() {
        return this.quantity;
    }
    setId(id) {
        this.id = id;
    }
    setQuantity(quantity) {
        this.quantity = quantity;
    }
    addQuantity(amount) {
        this.quantity += amount;
    }
    removeQuantity(amount) {
        this.quantity -= amount;
    }
    removeAllQuantity() {
        this.quantity = 0;
    }
    domRepresentation() {
        let cupImg = document.createElement("img");
        cupImg.src = "./assets/img/cup.png";
        cupImg.className = "cup";
        cupImg.id = this.getId().toString();
        return cupImg;
    }
}
class Game {
    constructor() {
        this.cups = [];
        this.clickHandler = (e) => {
            const cupId = Number(e.target.id);
            this.fillBucket(cupId);
        };
        this.cups[0] = new Cup(200, 0);
        this.cups[1] = new Cup(200, 1);
        this.cups[2] = new Cup(200, 2);
        this.cups[3] = new Cup(200, 3);
        this.cups[4] = new Cup(200, 4);
        this.cups[5] = new Cup(200, 5);
        this.cups[6] = new Cup(200, 6);
        this.cups[7] = new Cup(200, 7);
        const cupContainer = document.getElementById("waterreservoir");
        cupContainer.addEventListener("click", this.clickHandler);
        this.bucket = new Bucket(0, 5000);
        this.render();
    }
    render() {
        const bucketContainer = document.getElementById("watercontainer");
        bucketContainer.innerHTML = '';
        let bucketDiv = this.bucket.domRepresentation();
        bucketContainer.appendChild(bucketDiv);
        const cupContainer = document.getElementById("waterreservoir");
        cupContainer.innerHTML = '';
        this.cups.map((cup) => {
            cupContainer.appendChild(cup.domRepresentation());
        });
        console.log(this.cups);
    }
    getCupFromArray(cupIdFromTheDom) {
        let index = this.cups.findIndex((cup) => {
            console.log(cup.getId());
            return cup.getId() == cupIdFromTheDom;
        });
        console.log(index);
        return this.cups[index];
    }
    removeCupFromArray(cupIndexFromArray) {
        let index = this.cups.findIndex((cup) => {
            return cup.getId() == cupIndexFromArray;
        });
        this.cups.splice(index, 1);
    }
    fillBucket(cupId) {
        const cupQuantity = this.getCupFromArray(cupId).getQuantity();
        this.bucket.addQuantity(cupQuantity);
        this.removeCupFromArray(this.getCupFromArray(cupId).getId());
        this.render();
    }
}
let init = function () {
    let app = new Game();
};
window.addEventListener('load', init);
class View {
    constructor(numberOfCups = 0, cupQuantity = 0) {
        this.cups = [];
        for (let index = 1; index <= numberOfCups; index++) {
            this.cups[index] = new Cup(cupQuantity, index);
        }
        console.log(this.cups);
    }
    renderCups(HTMLPosition) {
        const cupView = `
        <div class="cups">
            ${this.cups.map(cup => `<img id=${cup.getId()} src="./assets/img/cup.png" onclick="game.cupOnClick(${cup.getId()});" />`).join("")}
        </div>`;
        HTMLPosition.innerHTML = cupView;
    }
    getCupById(index) {
        return this.cups[index];
    }
    renderBucket(bucketStartQuantity, bucketLimit, HTMLPBucketosition) {
        this.bucket = new Bucket(bucketStartQuantity, bucketLimit);
        let bucketView = document.createElement("div");
        bucketView.setAttribute("class", "bucket");
        HTMLPBucketosition.appendChild(bucketView);
    }
    getBucket() {
        return this.bucket;
    }
}
//# sourceMappingURL=main.js.map