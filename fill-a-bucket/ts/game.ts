class Game {

    private cups: Cup[] = [];
    private bucket: Bucket;

    constructor() {

        this.cups[0] = new Cup(200, 0);
        this.cups[1] = new Cup(200, 1);
        this.cups[2] = new Cup(200, 2);
        this.cups[3] = new Cup(200, 3);
        this.cups[4] = new Cup(200, 4);
        this.cups[5] = new Cup(200, 5);
        this.cups[6] = new Cup(200, 6);
        this.cups[7] = new Cup(200, 7);

        const cupContainer = document.getElementById("waterreservoir");
        cupContainer.addEventListener("click", this.clickHandler);

        this.bucket = new Bucket(0, 5000);

        this.render();
    }

    public render(): void {
        //bucket render

        const bucketContainer = document.getElementById("watercontainer");
        bucketContainer.innerHTML = '';

        let bucketDiv = this.bucket.domRepresentation();
        bucketContainer.appendChild(bucketDiv);

        //cup render
        const cupContainer = document.getElementById("waterreservoir");
        cupContainer.innerHTML = '';

        this.cups.map((cup) => {
            cupContainer.appendChild(cup.domRepresentation());
        });

        console.log(this.cups);
    }

    public clickHandler = (e: MouseEvent) => {
        const cupId = Number((<Element>e.target).id);
        this.fillBucket(cupId);
    }

    public getCupFromArray(cupIdFromTheDom: number): Cup {
        let index = this.cups.findIndex((cup) => {
            console.log(cup.getId());
            return cup.getId() == cupIdFromTheDom;
        });
        console.log(index);
        return this.cups[index];
    }

    public removeCupFromArray(cupIndexFromArray: number): void {
        let index = this.cups.findIndex((cup) => {
            return cup.getId() == cupIndexFromArray;
        });
        this.cups.splice(index, 1);
    }

    public fillBucket(cupId: number): void {

        //Get quantity from the cup
        const cupQuantity = this.getCupFromArray(cupId).getQuantity();

        //Add water to bucket from the cup
        this.bucket.addQuantity(cupQuantity);

        //let cup = this.getCupFromArray(cupId);

        //remove cup domelement
        this.removeCupFromArray(this.getCupFromArray(cupId).getId());

        //render screen
        this.render();
    }








}