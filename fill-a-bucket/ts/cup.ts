class Cup {

    private id: number;
    private quantity: number;
    private path: string;

    constructor(quantity: number, id: number) {
        this.setQuantity(quantity);
        this.setId(id);
    }

    public getId(): number {
        return this.id;
    }

    public getQuantity(): number {
        return this.quantity;
    }

    public setId(id: number): void {
        this.id = id;
    }

    public setQuantity(quantity: number) {
        this.quantity = quantity;
    }

    public addQuantity(amount: number) {
        this.quantity += amount;
    }

    public removeQuantity(amount: number) {
        this.quantity -= amount;
    }

    public removeAllQuantity() {
        this.quantity = 0;
    }

    public domRepresentation(): HTMLImageElement {
        let cupImg = document.createElement("img");
        cupImg.src = "./assets/img/cup.png";
        cupImg.className = "cup";
        cupImg.id = this.getId().toString();
        return cupImg;
    }

}