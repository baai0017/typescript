/**
 * 
 */

class Bucket {

    private quantity: number;
    private path: string;
    private limit: number;

    /**
     * 
     * @param quantity 
     * @param limit 
     */
    constructor(quantity: number, limit: number) {
        this.setQuantity(quantity);
        this.setLimit(limit);
    }

    /**
     * 
     */
    public getQuantity(): number {
        return this.quantity;
    }

    /**
     * 
     * @param bucketQuantity
     */
    public setQuantity(bucketQuantity: number) {
        this.quantity = bucketQuantity;
    }

    /**
     * 
     * @param bucketLimit
     */
    public setLimit(bucketLimit: number) {
        this.limit = bucketLimit;
    }

    /**
     * 
     * @param cupAmount
     */
    public addQuantity(cupAmount: number): string {
        if (this.checkBucketLimit(cupAmount)) {
            this.quantity += cupAmount;
            return 'There is ' + cupAmount + 'ml added to the bucket.';
        } else {
            console.log('Bucket is too full!');
            return 'Bucket is too full!';
        }
    }

    /**
     * @returns boolean
     */
    public checkBucketLimit(cupAmount: number): boolean {
        if (cupAmount <= (this.limit - this.quantity)) {
            return true;
        }
        return false;
    }

    /**
     * 
     * @param bucketAmount
     */
    public removeQuantity(bucketAmount: number) {
        this.quantity -= bucketAmount;
    }

    /**
     * 
     */
    public domRepresentationWater(): HTMLElement {
        let waterDiv = document.createElement("div");
        waterDiv.className = 'water';
        waterDiv.style.height = (this.getQuantity() / 25) + 'px';
        return waterDiv;
    }

    /**
     * 
     */
    public domRepresentation(): HTMLDivElement {

        let bucketDiv = document.createElement("div");
        bucketDiv.className = "bucket";

        let waterDiv = this.domRepresentationWater();
        bucketDiv.appendChild(waterDiv);

        return bucketDiv;
    }

}