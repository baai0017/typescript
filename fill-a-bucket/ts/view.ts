class View {

    private cups: Cup[] = [];
    private bucket: Bucket;

    constructor(numberOfCups: number = 0, cupQuantity: number = 0) {

        for (let index = 1; index <= numberOfCups; index++) {
            this.cups[index] = new Cup(cupQuantity, index);
        }

        console.log(this.cups);

    }

    public renderCups(HTMLPosition: HTMLElement) {

        const cupView = `
        <div class="cups">
            ${this.cups.map(
                cup => `<img id=${cup.getId()} src="./assets/img/cup.png" onclick="game.cupOnClick(${cup.getId()});" />`
            ).join("")}
        </div>`;

        HTMLPosition.innerHTML = cupView;

    }

    public getCupById(index: number): Cup {
        return this.cups[index];
    }

    public renderBucket(bucketStartQuantity: number, bucketLimit: number, HTMLPBucketosition: HTMLElement) {
        this.bucket = new Bucket(bucketStartQuantity, bucketLimit);

        let bucketView = document.createElement("div");
        bucketView.setAttribute("class", "bucket");

        HTMLPBucketosition.appendChild(bucketView);

    }

    public getBucket(): Bucket {
        return this.bucket;
    }

}