let init = function () {
    let app = new Game();
    let button = document.getElementById("btn");
    button.addEventListener("click", app.startGame);
};

window.addEventListener('load', init);

