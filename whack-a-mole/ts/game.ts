class Game {

    private holes: NodeList; 
    private moles: NodeList;

    private lastHole: Node;
    private hole: Node;
    private timeUp: boolean;
    private score: number;

    constructor() {
        this.timeUp = false;
        this.score = 0;

        this.holes = document.querySelectorAll('.hole');
        this.moles = document.querySelectorAll('.mole');

        this.moles.forEach(mole => mole.addEventListener('click', this.bonk));
    }

    public randTime(min: number, max: number): number {
        return Math.round(Math.random() * (max - min) + min);
    }

    public randomHole(holes: NodeList): Node {
        const idx = Math.floor(Math.random() * holes.length);
        const hole = holes[idx];

        if (hole == this.lastHole) {
            console.log("Overlapping same hole, trying again.");
            return this.randomHole(holes);
        }

        this.lastHole = hole;

        return hole;
    }

    public peep(): void {
        const time = this.randTime(200, 1000);
        this.hole = this.randomHole(this.holes);

        console.log(time, this.hole);

        (<HTMLElement>this.hole).classList.add("up");

        setTimeout(() => {
            (<HTMLElement>this.hole).classList.remove("up");
            if (!this.timeUp) {
                this.peep();
            }
        }, time);

    }

    public startGame = () => {

        document.querySelector('.score').innerHTML = "0";

        this.timeUp = false;
        this.score = 0;
        this.peep();

        setTimeout(() => {
            this.timeUp = true
        }, 10000);

    }


    public bonk = (e: MouseEvent) => {

        if (!e.isTrusted) {
            return;
        }

        (<HTMLElement>this.hole).classList.remove("up");

        this.score++;
        document.querySelector('.score').innerHTML = this.score.toString();
    }

}