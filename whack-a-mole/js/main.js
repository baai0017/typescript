class Game {
    constructor() {
        this.startGame = () => {
            document.querySelector('.score').innerHTML = "0";
            this.timeUp = false;
            this.score = 0;
            this.peep();
            setTimeout(() => {
                this.timeUp = true;
            }, 10000);
        };
        this.bonk = (e) => {
            if (!e.isTrusted) {
                return;
            }
            this.hole.classList.remove("up");
            this.score++;
            document.querySelector('.score').innerHTML = this.score.toString();
        };
        this.timeUp = false;
        this.score = 0;
        this.holes = document.querySelectorAll('.hole');
        this.moles = document.querySelectorAll('.mole');
        this.moles.forEach(mole => mole.addEventListener('click', this.bonk));
    }
    randTime(min, max) {
        return Math.round(Math.random() * (max - min) + min);
    }
    randomHole(holes) {
        const idx = Math.floor(Math.random() * holes.length);
        const hole = holes[idx];
        if (hole == this.lastHole) {
            console.log("Overlapping same hole, trying again.");
            return this.randomHole(holes);
        }
        this.lastHole = hole;
        return hole;
    }
    peep() {
        const time = this.randTime(200, 1000);
        this.hole = this.randomHole(this.holes);
        console.log(time, this.hole);
        this.hole.classList.add("up");
        setTimeout(() => {
            this.hole.classList.remove("up");
            if (!this.timeUp) {
                this.peep();
            }
        }, time);
    }
}
let init = function () {
    let app = new Game();
    let button = document.getElementById("btn");
    button.addEventListener("click", app.startGame);
};
window.addEventListener('load', init);
//# sourceMappingURL=main.js.map